Drupal.behaviors.textfieldfill = function(context) {
    
  $('.text-auto-fill').click(function() {
  
    $area = $(this).prev().find(':input');
    var id = $area.attr('id');

    // CKeditor checks
    if(typeof(CKEDITOR) !== 'undefined'){
      if (isEmpty(CKEDITOR.instances) !== true) {
        if (CKEDITOR.instances[id] !== 'undefined') {
          var oldText = CKEDITOR.instances[id].getData();
          CKEDITOR.instances[id].setData(oldText + $(this).text());
        }
      }
    }
    
    // Append to the html form item
    $area.val($area.val() + $(this).text());
    
  });
 
  // Check if an object is empty
  function isEmpty(obj) {
    for(var prop in obj) {
      if(obj.hasOwnProperty(prop))
        return false;
      }
    return true;
  }

  
}